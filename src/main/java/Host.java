package main.java;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author aleksruci on 17/Dez/2018
 * <p>
 * Host is the parent class for the Server and Client class.
 * Every host is described by a port number and a socket.
 * It also has capabilities of reading input from console
 * and transfering data from/to another host using data streams.
 * Method runHost() must be implemented by every host inheriting
 * from this class. It should provide the logic the host follows when
 * being run.
 */
public abstract class Host {
    private static final int RANDOM_PORT_NUMBER = 0; // Port number 0 inputStream ServerSocket results inputStream the server listening on a random, unused port

    private int port = RANDOM_PORT_NUMBER;
    private Socket socket;
    private Scanner scanner;
    private BufferedReader inputStream;
    private DataOutputStream outputStream;

    public Host(int port) {
        setPort(port);
    }

    public abstract void runHost() throws IOException;

    public void closeSocket() throws IOException {
        if(socket != null) {
            getSocket().close();
            System.out.println("Socket of address" + getSocket().getRemoteSocketAddress() + " successfully closed.\n");
        }
    }

    //Streaming Util methods are self descriptive (name of the method)

    public void startStream() throws IOException {
        startInputStream();
        startOutputStream();
    }

    public void closeStream() throws IOException {
        closeInputStream();
        closeOutputStream();
    }

    public void startInputStream() throws IOException {
        inputStream =  new BufferedReader(new InputStreamReader(getSocket().getInputStream()));
    }

    public void startOutputStream() throws IOException {
        outputStream = new DataOutputStream(getSocket().getOutputStream());
    }

    public void closeOutputStream() throws IOException {
        if(outputStream != null) {
            outputStream.close();
        }
    }

    public void closeInputStream() throws IOException {
        if(inputStream != null) {
            inputStream.close();
        }
    }

    public void startScanner() {
        scanner = new Scanner(System.in);
    }

    public void closeScanner() {
        if(scanner != null){
            scanner.close();
        }
    }

    //Getters and Setters

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public BufferedReader getInputStream() {
        return inputStream;
    }

    public void setInputStream(BufferedReader inputStream) {
        this.inputStream = inputStream;
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }

    public void setOutputStream(DataOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }
}
