package main.java;

/**
 * @author aleksruci on 23/Jan/2019
 */
public class Status {
    public static final String SUCCESS = "200 OK: Ready";
    public static final String INVALID_MEASUREMENT = "404 ERROR: Invalid Measurement Message";
    public static final String INVALID_CONNECTION_SETUP = "404 ERROR: Invalid Connection Setup Message";
}
