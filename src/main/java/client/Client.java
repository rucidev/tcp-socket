package main.java.client;

import main.java.Host;
import main.java.Status;
import main.java.server.Server;
import main.java.server.ServerValidator;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author aleksruci on 17/Dez/2018
 * <p>
 * This class provides the implementation of a client side as required.
 * Since the requirements specify a TCP connection established between
 * the server and the client an attribute "address" is used to specify
 * the IP address of the server to be connected to.
 */
public class Client extends Host {
    private String address;

    public Client(String address, int port) {
        super(port);
        this.address = address;
    }

    @Override
    public void runHost() throws IOException {
        try {
            runClient();
            startScanner(); //initialize scanner to read user input form the console
            startStream(); //initialize streaming objects
            echo();
        } catch (IOException e) {
            System.err.println("Server with address " + address + " and port number " + getPort() + " is not running or has problems.");
        } finally {
            closeScanner(); //close the scanner object
            closeStream(); //close the objects that stream data to/from the server
            closeSocket(); //close the socket connected to the server
        }
    }

    private void echo() throws IOException {
        getOutputStream().writeBytes(getConnectionMessage());
        String connectionResponse = getInputStream().readLine();
        System.out.println("Connection response: " + connectionResponse);
        getScanner().nextLine(); // to avoid \n in buffer

        if (connectionResponse.equals(Status.SUCCESS)) {
            String msg = getMessage();
            for (int i = 0; i < nrProbes; i++) {
                long sentTime = sendMessage(msg, i + 1); //time at which msg was sent
                String msgResponse = getInputStream().readLine();
                long receivedTime = System.nanoTime(); //time at which response is taken
                double rtt = getTravelTime(sentTime, receivedTime);

                System.out.println("Echo from server: " + msgResponse); //echo msg sent to server

                if (measurementType.equals(Server.MEASUREMENT_RTT)) {
                    total += rtt; //add rtt to total rtt
                    System.out.println("RTT for probe " + (i + 1) + " is " + rtt + " sec");
                } else if (measurementType.equals(Server.MEASUREMENT_TPUT)) {
                    double tput = (double) msgSize / rtt;
                    this.total += tput; //add tput to total tput
                    System.out.println("Throughput for probe " + (i + 1) + " is " + tput + " Bps");
                }
                if (msgResponse.equals("404 ERROR: Invalid Measurement Message")) {
                    System.out.println("Invalid Message sent to server.");
                    return;
                }

            }
            //calculate avg rtt or tput
            if (measurementType.equals(Server.MEASUREMENT_RTT)) {
                System.out.println("Average RTT is " + (total / nrProbes) + " sec \n");
            } else if (measurementType.equals(Server.MEASUREMENT_TPUT)) {
                System.out.println("Average Throughput is: " + (total / nrProbes) + " Bps \n");
            }
        }
    }

    //Initialize socket of the Client to connect with given IP address and port number
    private void runClient() throws IOException {
        setSocket(new Socket(address, getPort()));
        System.out.println("Client with address: " + getSocket().getLocalSocketAddress().toString() + " successfully created.");
        System.out.println("Successfully connected to server with address: " + getSocket().getRemoteSocketAddress() + ".");

    }

    private long sendMessage(String msg, int sequence) throws IOException {
        System.out.println("Enter protocol phase: (" + Server.PROTOCOL_PHASE + ")");
        String protocolPhase = getScanner().nextLine();
        long sentTime = System.nanoTime(); //time at which msg is sent
        getOutputStream().writeBytes(protocolPhase + " " + sequence + " " + msg + "\n"); //messages are concatenated in one message
        return sentTime;
    }

    private String getConnectionMessage() throws IOException {
        System.out.println("Enter protocol for connection setup: (" + Server.PROTOCOL_START + ")");
        String setup = getScanner().nextLine().toLowerCase();
        System.out.println("Enter measurement type: (" + Server.MEASUREMENT_RTT + " or " + Server.MEASUREMENT_TPUT + ")");
        measurementType = new Scanner(System.in).nextLine().toLowerCase();

        System.out.println("Enter number of probes (>" + ServerValidator.MIN_NR_PROBES + ")");
        nrProbes = getScanner().nextInt();

        if (measurementType.equals(Server.MEASUREMENT_RTT)) {
            System.out.println("Enter size of the message in bytes: (1, 100, 200, 400, 800 or 1000)");
            msgSize = getScanner().nextInt();
        } else if (measurementType.equals(Server.MEASUREMENT_TPUT)) {
            System.out.println("Enter size of the message in bytes: (1K, 2K, 4K, 8K, 16K or 32K)");
            msgSize = getScanner().nextInt();
        }

        return setup + " " + measurementType + " " + nrProbes + " " + msgSize + "\n"; //messages are concatenated in one message
    }

    //random message as an array of bytes of a specified length
    private String getMessage() throws UnsupportedEncodingException {
        byte[] bytes = new byte[msgSize];
        Arrays.fill(bytes, (byte) 't');
        String msg = new String(bytes, "UTF-8");
        return msg;
    }

    private double getTravelTime(long sentTime, long receivedTime) {
        return (receivedTime - sentTime) / 1000000000.0; //because coeff nano to sec is e-9
    }

    private int nrProbes;
    private int msgSize;
    private String measurementType;
    private double total = 0;
}
