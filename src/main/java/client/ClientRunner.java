package main.java.client;

import java.io.IOException;

/**
 * @author aleksruci on 17/Dez/2018
 * <p>
 * Runner class for the Client
 */
public class ClientRunner {
    public static void main(String[] args) throws IOException {
        Client client = new Client("127.0.0.1", 4444);
        client.runHost();
    }
}
