package main.java.server;

import main.java.Host;
import main.java.Status;

import java.io.*;
import java.net.ServerSocket;

/**
 * @author aleksruci on 17/Dez/2018
 * <p>
 * This class gives the implementation of the server side host
 * It has a server socket which is used to initialize the socket of this host
 * when a client has been accepted and thus connected. It echoes back every
 * message that flows towards it from the client.
 */
public class Server extends Host {
    public static final long SERVER_DELAY = 2000; // in milli seconds
    public static final String PROTOCOL_START = "s";
    public static final String PROTOCOL_PHASE = "m";
    public static final String MEASUREMENT_RTT = "rtt";
    public static final String MEASUREMENT_TPUT = "tput";

    private ServerSocket serverSocket;

    public Server(int port) {
        super(port);
    }

    @Override
    public void runHost() throws IOException {
        runServer();
        while (true) {      //keep the server running all the time even if the current client decides to disconnect
            try {
                acceptClientSocket();   //accept client
                startStream();      //initialize streaming objects
                echo();      //stream data with the client
            } catch (IOException e) {
                //in case a client is shut down or another error happens, the server will not stop running
                //the exception is thrown because in that case the streaming of data is going to stop and
                //the client will stop sending any messages
                System.err.println("Client " + getSocket().getRemoteSocketAddress() + " shutdown or had unexpected error.");
            } catch (InterruptedException e) {
                System.err.println("Interrupted! Closing connection");
            } finally {
                closeStream();
                closeSocket();   //close socket of that specific client
            }
        }
    }

    private void echo() throws IOException, InterruptedException {
        readConnectionVariables(); //reads the first argument of inputs
        if (!areConnectionVariablesValid()) {
            System.out.println("Connection variables not valid! Connection terminating...");
            return;
        } else {
            getOutputStream().writeBytes(Status.SUCCESS + "\n");
        }
        for (int i = 0; i < nrProbes; i++) {
            readMsgVariables(); //reads the second argument of inputs
            delayServer();

            int sequence = i + 1;
            if (ServerValidator.isProtocolPhaseValid(protocolPhase) && probe == sequence && msg.length() == msgSize) {
                getOutputStream().writeBytes("Echoing msg with probe number " + probe + " and size " + msg.length() + "\n");
            } else {
                getOutputStream().writeBytes(Status.INVALID_MEASUREMENT + "\n");
                return;
            }
        }
    }

    //run the server with the given port number
    private void runServer() throws IOException {
        serverSocket = new ServerSocket(getPort());
        System.out.println("Server connected to address: " + serverSocket.getLocalSocketAddress().toString() + " running. Waiting for a client connection...");
    }

    //wait until a client is accepted to connect by the server socket
    private void acceptClientSocket() throws IOException {
        setSocket(serverSocket.accept());
        System.out.println("\nClient with address: " + getSocket().getRemoteSocketAddress().toString() + " successfully connected.");
    }

    //reads and assigns connection variables taken from client
    private void readConnectionVariables() throws IOException {
        String[] inputs = getInputStream().readLine().split(" ");
        if (inputs.length != 4) {
            System.out.println("Invalid input entered!");
            return;
        }
        protocol = inputs[0];
        measurement = inputs[1];
        nrProbes = Integer.parseInt(inputs[2]);
        msgSize = Integer.parseInt(inputs[3]);
    }

    //reads and assigns message variables taken from client
    private void readMsgVariables() throws IOException {
        String[] inputs = getInputStream().readLine().split(" ");
        if (inputs.length != 3) {
            System.out.println("Invalid input entered!");
            return;
        }
        protocolPhase = inputs[0];
        probe = Integer.parseInt(inputs[1]);
        msg = inputs[2];
    }

    private boolean areConnectionVariablesValid() throws IOException {
        if (!ServerValidator.isProtocolValid(protocol) || !ServerValidator.isMeasurementValid(measurement) || !ServerValidator.isNumberProbesValid(nrProbes) || !ServerValidator.isMsgSizeValid(measurement, msgSize)) {
            getOutputStream().writeBytes(Status.INVALID_CONNECTION_SETUP + "\n");
            return false;
        }
        return true;
    }

    private void delayServer() throws InterruptedException {
        Thread.sleep(SERVER_DELAY);
    }

    //connection variables
    private String protocol = "";
    private String measurement = "";
    private int nrProbes = 0;
    private int msgSize = 0;

    //message variables
    private String protocolPhase = "";
    private int probe = 0;
    private String msg = "";
}