package main.java.server;

import java.io.IOException;

/**
 * @author aleksruci on 17/Dez/2018
 * <p>
 * Runner class for the Server
 */
public class ServerRunner {

    public static void main(String args[]) throws IOException {
        Server server = new Server(4444);
        server.runHost();
    }
}
