package main.java.server;

import static main.java.server.Server.*;

/**
 * @author aleksruci on 23/Jan/2019
 * A util class which is used by the server
 * to validate its specific variables
 */
public class ServerValidator {
    public static final int MIN_NR_PROBES = 10;


    public static boolean isProtocolValid(String protocol) {
        return protocol.equals(PROTOCOL_START);
    }

    public static boolean isMeasurementValid(String measurement) {
        return measurement.equals(MEASUREMENT_RTT) || measurement.equals(MEASUREMENT_TPUT);
    }

    public static boolean isNumberProbesValid(int nrProbes) {
        return nrProbes >= MIN_NR_PROBES;
    }

    public static boolean isMsgSizeValid(String measurement, int size) {
        if (measurement.equals(MEASUREMENT_RTT) & (size == 1 || size == 100 || size == 200 || size == 400 || size == 800 || size == 1000)) {
            return true;
        } else if (measurement.equals(MEASUREMENT_TPUT) & (size == 1000 || size == 2000 || size == 4000 || size == 8000 || size == 16000 || size == 32000)) {
            return true;
        }
        return false;
    }

    public static boolean isProtocolPhaseValid(String protocolPhase) {
        return protocolPhase.equals(PROTOCOL_PHASE);
    }
}
